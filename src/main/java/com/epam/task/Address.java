package com.epam.task;

public class Address {
    private int floorNo;
    private String streetName;
    private String cityName;
    private String state;
    private String country;

    public Address(int floorNo, String streetName, String cityName, String state, String country) {
        this.floorNo = floorNo;
        this.streetName = streetName;
        this.cityName = cityName;
        this.state = state;
        this.country = country;
    }

    public int getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(int floorNo) {
        this.floorNo = floorNo;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Address{" +
                "floorNo=" + floorNo +
                ", streetName='" + streetName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
