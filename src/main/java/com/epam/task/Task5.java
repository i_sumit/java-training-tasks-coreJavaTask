package com.epam.task;

import java.util.*;

public class Task5 {
    public static void main(String[] args) {
        HashMap<String,Integer> days = new HashMap<>();
        days.put("Monday",1);
        days.put("Tuesday",2);
        days.put("Wednesday",3);
        days.put("Thursday",4);
        days.put("Friday",5);
        days.put("Saturday",6);
        days.put("Sunday",7);
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter current day: ");
        String currentDay = sc.next();
        if(!days.containsKey(currentDay)){
            System.out.println("please enter valid day");
            System.exit(0);
        }
        System.out.print("After K days. K? : ");
        int nextDay = sc.nextInt();
        if(nextDay<0) {
            System.out.println("Next day should be greater than 0");
        }
        nextDay = nextDay > 7 ? nextDay%7  : nextDay;
        int afterK_days = nextDay + days.get(currentDay) > 7 ? (nextDay + days.get(currentDay))%7 : nextDay + days.get(currentDay);
        for(Map.Entry<String,Integer> m: days.entrySet()){
            if(m.getValue()==afterK_days)
                System.out.println(m.getKey());
        }
    }
}
