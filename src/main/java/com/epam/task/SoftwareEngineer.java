package com.epam.task;

public class SoftwareEngineer extends Employee {
    private String projectName;

    public SoftwareEngineer(int id, String name, double salary, Address address, String projectName) {
        super(id, name, salary, address);
        this.projectName = projectName;
    }

    @Override
    public void printEmployeeDetails(){};

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
