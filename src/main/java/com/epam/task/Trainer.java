package com.epam.task;

import java.util.List;

public class Trainer extends Employee {
    private List<String> skills;
    private List<String> certifications;

    public Trainer(int id, String name, double salary, Address address, List<String> skills, List<String> certifications) {
        super(id, name, salary, address);
        this.skills = skills;
        this.certifications = certifications;
    }

    @Override
    public void printEmployeeDetails(){};

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public List<String> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<String> certifications) {
        this.certifications = certifications;
    }
}
