package com.epam.task;

public class JuniorEngineer extends Employee {
    private int assignmentScore;
    private String feedback;

    public JuniorEngineer(int id, String name, double salary, Address address, int assignmentScore, String feedback) {
        super(id, name, salary, address);
        this.assignmentScore = assignmentScore;
        this.feedback = feedback;
    }

    public int getAssignmentScore() {
        return assignmentScore;
    }

    public void setAssignmentScore(int assignmentScore) {
        this.assignmentScore = assignmentScore;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
